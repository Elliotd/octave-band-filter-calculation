# Octave band calculation 

This calculates the magnitude of octave band for the specific parameters (Nth octave, sampling frequency, fft size) to apply to a calculation of an FFT.
