clearvars;
close all;
addpath("A-weighting_filter")




fs = 48000;
lengthSeconds = 10;
time = (1:(lengthSeconds*))/fs;
signal1 = sin(time*2*pi*1000);
signal2 = sin(time*2*pi*500);

signal1A = filterA(signal1,fs);
signal2A = filterA(signal2,fs);

db1 = 20*log10(rms(signal1));
db2 = 20*log10(rms(signal2));

dbA1 = 20*log10(rms(signal1A));
dbA2 = 20*log10(rms(signal2A));
