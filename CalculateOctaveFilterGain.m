close all;
clearvars;

%% Settings

fftBlockSize = 1024;
fs = 48000;
% Valid values are: "1 octave" | "2/3 octave" |"1/2 octave" | "1/3 octave" | "1/6 octave" | "1/12 octave" | "1/24 octave" | "1/48 octave".
octaveBands = '1/3 octave';


% Boundaries
minFreq = 20;
maxFreq = 20000;


%% Center frequency of octave band
fcenter  = 10^3 * (2 .^ ([-18:13]/3));
% We take the same as the poctave to simplify our lives
[~,fcenter] = poctave(zeros(2),fs,'BandsPerOctave',3);

% We avoid asking a center frequency above the maximum fftbin
fcenter = fcenter(minFreq<fcenter & fcenter<maxFreq & fcenter<fs/2);

filters = nan(fftBlockSize/2 +1, length(fcenter));
figure;
for i=1:length(fcenter)
    octFilt = octaveFilter(fcenter(i),octaveBands,'SampleRate',fs);
    [filters(:,i),f] = freqz(octFilt, fftBlockSize/2 +1, fs);
    semilogx(f, pow2db(abs(filters(:,i))));
    hold on;
end

filters = abs(filters);
save('filters.mat', 'filters','fcenter');

writematrix(filters,'filters.csv')
writematrix(filters.^2,'filtersSquared.csv')


%% A-weiting calculation for the bands

% taken from Aweighting filter joined in the repo

% A-weighting filter coefficients
c1 = 12194.217^2;
c2 = 20.598997^2;
c3 = 107.65265^2;
c4 = 737.86223^2;

% evaluate the A-weighting filter in the frequency domain
freq = fcenter.^2;
num = c1*(freq.^2);
den = (freq+c2) .* sqrt((freq+c3).*(freq+c4)) .* (freq+c1);
A = 1.2589*num./den;

figure
semilogx(freq, 2*pow2db(abs(A)));
writematrix(A,'AWeighting.csv')
writematrix(A.^2,'AWeightingSquared.csv')
