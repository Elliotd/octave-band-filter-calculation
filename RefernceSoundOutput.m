%%
close all;
clearvars;

%% List of all samples 

work_folder = 'SampleAudio/';

sample_names = {...
    '250Hz48000_16bit';...
    '1000Hz48000_16bit';...
    '6000Hz48000_16bit';...
    'WhiteNoise48000_16bit'...
    };


%% Go though all audio to get the octave bands

out_dB_REF = nan(length(sample_names),30);

for i=1:length(sample_names)
    [x,fs] = audioread(strcat(work_folder, sample_names{i}, '.wav'));

    [pREF,cf] = poctave(x,fs,'BandsPerOctave',3);

    % Get the values between 20 and 20k
    cond = cf>20;
    pREF = pREF(cond);
    cf = cf(cond);
    
    % Convert into dB with reference value of 20 micro pascal
    % Squared because p is a power
    pREF = pow2db(pREF/(0.00002^2));
    
    out_dB_REF(i,:) = pREF;
    csvwrite(strcat(sample_names{i}, 'REF.csv'), pREF)
end
csvwrite('cfREF.csv', cf)

%% Go through the same thing with fft


out_dB = nan(length(sample_names),30);

for i=1:length(sample_names)
    [x,fs] = audioread(strcat(work_folder, sample_names{i}, '.wav'));

    nfft = 1024;
    % Warning there is windowing here, but we need to check with IOS to see
    % if there is windowing
    % Here we are estimating the power spectral density (dB/Hz) but at the
    % end of the day we will want the power (dB), not power density (db/Hz)
    [pxx,f] = pwelch(x,hanning(nfft),0,nfft,fs);

    load('filters.mat');
    
    % Matrix operation (you can ignore the f(2)-f(1) as this is to
    % counteract the dB/Hz of the pwelch method
    % Pxx is 1x513, filters is 513x27
    filtered = pxx'*(filters.^2)* (f(2)-f(1));
    % Switching to dB with 20 micropascal reference.
    p = pow2db(filtered/(0.00002^2));

    out_dB(i,:) = p;
    
    csvwrite(strcat(sample_names{i}, 'Out.csv'), p)
end


%% Plot result

figure
plot(cf, out_dB_REF-out_dB)
title("Comparison oct-band")
legend(sample_names);

figure
subplot(2,1,1)
plot(cf, out_dB_REF)
subplot(2,1,2)
plot(cf, out_dB)




