%%
close all;
clearvars;

%% List of all samples 

work_folder = 'Version6april/';


%% Calculate the calibration factor.

[x, fs] = audioread(strcat(work_folder,'Calib1K.wav'));
x = x(:,1);

calibrationFactor = calibrateMicrophone(x,fs,94);

%% Load the white noise


[x, fs] = audioread(strcat(work_folder,'WhiteNoise.wav'));
x = x(:,1);

[~,cf] = poctave(x,fs,'BandsPerOctave',3);
% Get the values between 20 and 20k
cond = cf>20;
cf = cf(cond);

nfft = 1024;
% Warning there is windowing here, but we need to check with IOS to see
% if there is windowing
% Here we are estimating the power spectral density (dB/Hz) but at the
% end of the day we will wafnt the power (dB), not power density (db/Hz)
[pxx,f] = pwelch(x,hanning(nfft),0,nfft,fs);

load('filters.mat');

% Matrix operation (you can ignore the f(2)-f(1) as this is to
% counteract the dB/Hz of the pwelch method
% Pxx is 1x513, filters is 513x27
filtered = pxx'*(filters.^2)* (f(2)-f(1));


%% A-weiting calculation for the bands

% taken from Aweighting filter joined in the repo

% A-weighting filter coefficients
c1 = 12194.217^2;
c2 = 20.598997^2;
c3 = 107.65265^2;
c4 = 737.86223^2;

% evaluate the A-weighting filter in the frequency domain
freq = fcenter.^2;
num = c1*(freq.^2);
den = (freq+c2) .* sqrt((freq+c3).*(freq+c4)) .* (freq+c1);
A = 1.2589*num./den;

figure
semilogx(fcenter, 2*pow2db(abs(A)));
writematrix(A,'AWeighting.csv')
writematrix(A.^2,'AWeightingSquared.csv')

filtered = filtered.*A';

% Switching to dB with 20 micropascal reference.
out_dB = pow2db(filtered/(0.00002^2));

%% Load CSV and compare

T = [-164.65514931310286783628;-146.42097827310044522164;-118.58051148938000096678;-105.44363367355465754827;-122.62877975293105237142;-98.87328326534138511761;-86.40240407757973173375;-83.30047048840134493730;-80.89009629159455982972;-74.68764453682841519822;-69.34556788941605987020;-66.53037105661903183318;-62.08790570087082016926;-62.16883107434700406202;-51.82461676261118554976;-52.50906330267810773194;-47.38046836795797389641;-52.54012499666646363039;-49.95342002387821622733;-49.24831745533041527096;-47.10890064779575681086;-46.86859179921022189319;-45.46613823640715423835;-43.63637991387449233116;-45.71139523187771658286;-45.01467082477275027941;-48.23293102587782499313;-50.42288314503964130608;-54.01416757378029132042;-66.10980711588054248296];

%% Calibrate for all frequenceies

Calib = out_dB-T';
CalibPow = db2pow(Calib);

csvwrite('calib.csv', CalibPow)




%% Plot result

figure
semilogx(cf, out_dB)
hold on
semilogx(cf, T + Calib(17))

figure
semilogx(cf, Calib)





