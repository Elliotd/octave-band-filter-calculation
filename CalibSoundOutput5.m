%%
close all;
clearvars;

% This is used after the calibration to validate if the correction is
% adequate.

%% List of all samples 

work_folder = 'Version29Novembre/';


%% Calculate the calibration factor.

[x, fs] = audioread(strcat(work_folder,'Calib1K.wav'));
x = x(:,1);

calibrationFactor = calibrateMicrophone(x,fs,94);

%% Load the white noise


[x1, fs] = audioread(strcat(work_folder,'Measure1.wav'));
x1 = x1(:,1);
[x2, ~] = audioread(strcat(work_folder,'Measure2.wav'));
x2 = x2(:,1);


[~,cf] = poctave(x1,fs,'BandsPerOctave',3);
% Get the values between 20 and 20k
cond = cf>20;
cf = cf(cond);

nfft = 1024;
% Warning there is windowing here, but we need to check with IOS to see
% if there is windowing
% Here we are estimating the power spectral density (dB/Hz) but at the
% end of the day we will wafnt the power (dB), not power density (db/Hz)
[pxx1,f] = pwelch(x1*calibrationFactor,hanning(nfft),0,nfft,fs);
[pxx2,~] = pwelch(x2*calibrationFactor,hanning(nfft),0,nfft,fs);

load('filters.mat');

% Matrix operation (you can ignore the f(2)-f(1) as this is to
% counteract the dB/Hz of the pwelch method
% Pxx is 1x513, filters is 513x27
filtered = (1/2)*(pxx1+pxx2)'*(filters.^2)* (f(2)-f(1));



%% A-weiting calculation for the bands

% taken from Aweighting filter joined in the repo

% A-weighting filter coefficients
c1 = 12194.217^2;
c2 = 20.598997^2;
c3 = 107.65265^2;
c4 = 737.86223^2;

% evaluate the A-weighting filter in the frequency domain
freq = fcenter.^2;
num = c1*(freq.^2);
den = (freq+c2) .* sqrt((freq+c3).*(freq+c4)) .* (freq+c1);
A = 1.2589*num./den;
Asquared = A.^2;

%figure
%semilogx(fcenter, 2*pow2db(abs(A)));
writematrix(A,'AWeighting.csv')
writematrix(A.^2,'AWeightingSquared.csv')

filtered = filtered.*Asquared';

dBALevel = pow2db(sum(filtered/(0.00002^2)));

% Switching to dB with 20 micropascal reference.
out_dB = pow2db(filtered/(0.00002^2));


%% Load CSV and compare

T1 = readmatrix(strcat(work_folder,'Measure1.csv'));
% Divide by X because it's the length of the measurement
T1 = pow2db(db2pow(T1)/26);
dBALevel2 = pow2db(sum(db2pow(T1)));
T2 = readmatrix(strcat(work_folder,'Measure2.csv'));
% Divide by X because it's the length of the measurement
T2 = pow2db(db2pow(T2)/26);
dBALevel3 = pow2db(sum(db2pow(T2)));
%% Plot result

figure
semilogx(cf, out_dB)
hold on
semilogx(cf, T1)
semilogx(cf, T2)

%figure
%semilogx(cf, Calib)

%% Calibrate for all frequenceies

Calib = out_dB-pow2db((db2pow(T1)+db2pow(T2))/2);


oldCalib = pow2db(readmatrix('calib.csv'));
oldCalib2 = pow2db(readmatrix('calib2.csv'));
oldCalib3 = pow2db(readmatrix('calib3.csv'));
oldCalib4 = pow2db(readmatrix('calib4.csv'));


newCalib = Calib;

figure
semilogx(cf, oldCalib)
hold on
semilogx(cf, oldCalib2)
semilogx(cf, oldCalib3)
semilogx(cf, oldCalib4)
semilogx(cf, newCalib)


CalibPow = db2pow(newCalib);
csvwrite('calib5.csv', CalibPow)


%% Testing new filter

T = pow2db((db2pow(T1)+db2pow(T2))/2) + Calib;
level = pow2db(sum(db2pow(T)));



